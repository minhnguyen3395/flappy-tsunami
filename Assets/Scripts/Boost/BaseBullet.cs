﻿using UnityEngine;
using System.Collections;
using Destructible2D;

public class BaseBullet : MonoBehaviour {
    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private float _speed;

    [SerializeField]
    private float _delayDestroy;

    private Transform _trans;
    private Vector3 _startingPosition;

    void Awake()
    {
        _trans = transform;
    }

    public void Move()
    {
        _startingPosition = _trans.position;
        StartCoroutine(MovingCor(-_trans.right, _speed));
    }

    private IEnumerator MovingCor(Vector3 dir, float speed)
    {
        while(true)
        {
            _trans.Translate(dir * speed * Time.deltaTime);
            yield return null;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if(TagCollection.Compare(other.tag, TagCollection.BOTTOM_GROUND) || TagCollection.Compare(other.tag, TagCollection.TOP_GROUND))
        {
            PoolFactory.Despawn(gameObject, _delayDestroy);
        }
        else if(TagCollection.Compare(other.tag, TagCollection.OBSTACLE))
        {
            //other.GetComponentInParent<Rigidbody2D>().isKinematic = false;
            PoolFactory.Spawn(_explosionPrefab, other.transform.position, Quaternion.identity);
        }
    }
}
