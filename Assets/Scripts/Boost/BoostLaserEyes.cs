﻿using UnityEngine;
using System.Collections;
using Destructible2D;

public class BoostLaserEyes : BaseBoost
{
    [SerializeField]
    private GameObject _laserPrefab;

    [SerializeField]
    private float _firingPeriod;

    [SerializeField]
    [Range(0f, 60f)]
    private float _firingAngleOffset;

    private WaitForSeconds _waitFiring;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void Awake()
    {
        base.Awake();
        _waitFiring = new WaitForSeconds(_firingPeriod);
    }

    protected override void Boost()
    {
        base.Boost();
        StartCoroutine(FireLaserCor());
    }

    private IEnumerator FireLaserCor()
    {
        while (true)
        {
            _groupBirdController.LoopMemberList((BirdController birdController, int index) =>
            {
                if (birdController == null)
                    return;

                Transform birdTrans = birdController.transform;
                BaseBullet bullet = PoolFactory.Spawn(_laserPrefab, birdTrans.position, Quaternion.Euler(0f, 0f, birdTrans.rotation.z - Random.Range(-_firingAngleOffset, _firingAngleOffset) - 90f)).GetComponent<BaseBullet>();
                bullet.Move();
            });
            yield return _waitFiring;
        }
    }
}
