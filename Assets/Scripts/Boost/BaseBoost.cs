﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseBoost : MonoBehaviour
{
    [SerializeField]
    private float _activeDuration;

    [SerializeField]
    private GameObject _transformationParticle;

    [SerializeField]
    private Sprite _transformationSprite;

    protected GroupController _groupBirdController;
    private bool _isActivated = false;
    private WaitForSeconds _waitSecs;

    protected virtual void OnEnable()
    {
        _isActivated = false;
    }

    protected virtual void Awake()
    {
        _groupBirdController = GameObject.FindGameObjectWithTag(TagCollection.GROUP_BIRD).GetComponent<GroupController>();
        _waitSecs = new WaitForSeconds(_activeDuration);
    }

	protected virtual void OnTriggerEnter2D (Collider2D other)
	{
        if (_isActivated)
            return;

		if (TagCollection.Compare (other.tag, TagCollection.BIRD)) {
            _isActivated = true;
            StartCoroutine(BoostCor());
		}
	}

    private IEnumerator BoostCor()
    {
        Boost();
        yield return _waitSecs;
        _groupBirdController.LoopMemberList((BirdController birdController, int index) =>
        {
            birdController.GraphicComponent.ResetGraphic();
        });
        PoolFactory.Despawn(gameObject);
    }

    protected virtual void Boost()
    {
        _groupBirdController.LoopMemberList((BirdController birdController, int index) =>
        {
            birdController.GraphicComponent.TransformationExecute(_transformationParticle, _transformationSprite);
        });
    }
}
