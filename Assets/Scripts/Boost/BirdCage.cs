﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class BirdCage : MonoBehaviour
{
    [SerializeField]
    private int _numberOfBird = 1;

    [SerializeField]
    private float _delayFlyOut = 0.5f;

    private Transform _trans;
    private WaitForSeconds _waitSecs;
    private bool _isActive = false;

    public static UnityAction<BirdController> OnCreateBirdFromCage;

    void Awake()
    {
        _trans = transform;
        _waitSecs = new WaitForSeconds(_delayFlyOut);
    }

    private void CreateBird(Vector3 targetPosition)
    {
        GameObject birdGO = ActorFactory.Instance.InstantiateBird(_trans.position);
        BirdController birdController = birdGO.GetComponent<BirdController>();
        birdController.MovementComponent.FlappyToPosition(targetPosition, 3f, .5f, 0.3f, delegate
        {
            if (OnCreateBirdFromCage != null)
            {
                if (OnCreateBirdFromCage != null)
                    OnCreateBirdFromCage(birdController);
            }
        });
    }

    void OnTriggerEnter2D(Collider2D other)
    {
		if (!TagCollection.Compare(other.tag, TagCollection.BIRD) || _isActive)
            return;

        _isActive = true;
        StartCoroutine(CreateBirdDelayCoroutine(_numberOfBird, other.transform.position));
    }

    private IEnumerator CreateBirdDelayCoroutine(int amount, Vector3 position)
    {
        for (int i = 0; i < amount; i++)
        {
            CreateBird(position);
            yield return _waitSecs;
        }

		Destroy (gameObject);
    }
}
