﻿using UnityEngine;
using System.Collections;

public class PositionAlignCamera : MonoBehaviour
{
	public enum Anchor
	{
		TOP,
		BOTTOM,
		LEFT,
		RIGHT,
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT
	}

	[SerializeField]
	private Anchor _anchor;

	[SerializeField]
	private Vector3 _offset;

	private static Vector2 _topLeftVec = new Vector2 (-1f, 1f);
	// Use this for initialization

	private ViewportHandler camFit;
	
	void Start ()
	{ 
		camFit = ViewportHandler.Instance;

		if (_anchor == Anchor.TOP) {
			SetPosition (Vector2.up);
		} else if (_anchor == Anchor.BOTTOM) {
			SetPosition (Vector2.down);
		} else if (_anchor == Anchor.RIGHT) {
			SetPosition (Vector2.right);
		} else if (_anchor == Anchor.LEFT) {
			SetPosition (Vector2.left);
		} else if (_anchor == Anchor.TOP_LEFT) {
			SetPosition (_topLeftVec);
		} else if (_anchor == Anchor.TOP_RIGHT) {
			SetPosition (Vector2.one);
		} else if (_anchor == Anchor.BOTTOM_LEFT) {
			SetPosition (-Vector2.one);
		} else if (_anchor == Anchor.BOTTOM_RIGHT) {
			SetPosition (-_topLeftVec);
		}
	}

	private void SetPosition (Vector2 anchorVec)
	{
		Vector3 pos = transform.position;

		if (anchorVec.x != 0)
			pos.x = anchorVec.x * camFit.Width / 2;

		if (anchorVec.y != 0)
			pos.y = anchorVec.y * camFit.Height / 2;

		pos = pos + _offset;
		transform.position = pos;
	}
}
