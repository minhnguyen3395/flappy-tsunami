﻿using UnityEngine;
using System.Collections;

public class TagCollection {
    public const string BIRD = "Player";
	public const string BOTTOM_GROUND = "BottomGround";
	public const string TOP_GROUND = "TopGround";
	public const string OBSTACLE = "Obstacle";
	public const string BOOST = "Boost";
    public const string GROUP_BIRD = "GroupBird";

	public static bool Compare(string tag1, string tag2)
	{
		return tag1.CompareTo (tag2) == 0;
	}
}
