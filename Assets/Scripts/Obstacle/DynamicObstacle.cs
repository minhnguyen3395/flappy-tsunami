﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DynamicObstacle : System.Object
{
	[SerializeField]
	private GameObject _movingPath;

	[SerializeField]
	private GameObject _obstacleGO;

	[SerializeField]
	private float _chanceActivate = 0.5f;

	[SerializeField]
	private float _movingTime;

    public GameObject MovingPathRoot { get { return _movingPath; } }

	public GameObject ObstacleGO { get { return _obstacleGO; } }

	public float ActiveChance { get { return _chanceActivate; } }

	public float MovingTime{ get { return _movingTime; } }
}
