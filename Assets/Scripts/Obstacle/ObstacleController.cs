﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour
{
    [SerializeField]
    private DynamicObstacle[] _dynamicObstacles;

    public virtual void StartMoving(Transform endPoint, float duration)
    {
        LeanTween.moveX(gameObject, endPoint.position.x - ViewportHandler.Instance.Width / 2, duration).setOnStart(() =>
        {
            StartCoroutine(StartDynamicObstacles(duration / 2));
        }).setOnComplete(() =>
        {
            PoolFactory.Despawn(gameObject);
        });
    }

    private IEnumerator StartDynamicObstacles(float duration)
    {
        yield return new WaitForSeconds(duration);
        for (int i = 0; i < _dynamicObstacles.Length; i++)
        {
            DynamicObstacle dynamicObstacle = _dynamicObstacles[i];
            Vector3[] points = GetMovingPath(dynamicObstacle);
            Vector3 startingPoint = points[0];
            GameObject obstacleGO = PoolFactory.Spawn(dynamicObstacle.ObstacleGO, startingPoint, Quaternion.identity);

            LTSpline spline = new LTSpline(points);
            LeanTween.move(obstacleGO, spline, dynamicObstacle.MovingTime).setEase(LeanTweenType.linear).setOnComplete(() =>
            {
                PoolFactory.Despawn(obstacleGO);
            });
        }
    }

    private Vector3[] GetMovingPath(DynamicObstacle dynamicOb)
    {
        Transform pathRoot = PoolFactory.Spawn(dynamicOb.MovingPathRoot).transform; 
        Transform[] movingPathTrans = new Transform[pathRoot.childCount];

        for(int i = 0; i < movingPathTrans.Length; i++)
        {
            movingPathTrans[i] = pathRoot.GetChild(i);
        }

        Vector3[] _movingPathVec;

        _movingPathVec = new Vector3[movingPathTrans.Length];
        for (int i = 0; i < _movingPathVec.Length; i++)
        {
            _movingPathVec[i] = movingPathTrans[i].position;
        }

        return _movingPathVec;
    }
}
