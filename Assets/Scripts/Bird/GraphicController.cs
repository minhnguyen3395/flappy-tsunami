﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class GraphicController : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private Animator _anim;
    private Sprite _originalSprite;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();
        _originalSprite = _spriteRenderer.sprite;
    }

    public void Die()
    {
        _spriteRenderer.color = Color.black;
        _anim.StopPlayback();
    }

    public void FlyUp()
    {
        _anim.SetBool("isFlap", true);
    }

    public void AnimationEventFlapToIdle()
    {
        _anim.SetBool("isFlap", false);
    }

    public void TransformationExecute(GameObject prefab, Sprite boostSprite)
    {
        PoolFactory.Spawn(prefab, gameObject);

        if (boostSprite != null)
            _spriteRenderer.sprite = boostSprite;
    }

    public void ResetGraphic()
    {
        _anim.StartPlayback();
        _spriteRenderer.sprite = _originalSprite;
    }
}
