﻿using UnityEngine;

public class FlappyMovement : MonoBehaviour
{
    [SerializeField]
    private float _forwardVecX = 5f;

    [SerializeField]
    private float _upwardVecY = 30f;

    [SerializeField]
    private float _maxVecY = 100f;

    [SerializeField]
    private float _fallingDownVecY = -20f;

    [SerializeField]
    private float _upwardAngle = 20f;

    [SerializeField]
    private float _rotatingUpSpeed = 1000f;

    [SerializeField]
    private float _downwardAngle = -90f;

    [SerializeField]
    private float _rotatingDownSpeed = 120f;

    [SerializeField]
    private Vector3 _acceleration = Vector3.zero;

    public Vector3 Position
    {
        get { return _trans.position; }
    }

    private Vector3 _velocity;
    private Vector3 _position;
    private Transform _trans;
    private GameObject _graphicGO;
    private Transform _graphicTrans;
    private float _maxHeightY;
    private float rotation;
    private bool _canFly = true;

    void Awake()
    {
        _trans = transform;
        _graphicTrans = _trans.GetChild(0);
        _graphicGO = _graphicTrans.gameObject;
        _canFly = true;
        _maxHeightY = Camera.main.orthographicSize;
    }

    public void Falling()
    {
        // Rotate up
        if (_velocity.y > 0)
        {
            rotation += _rotatingUpSpeed * Time.deltaTime;

            if (rotation > _upwardAngle)
            {
                rotation = _upwardAngle;
            }
        }

        // Rotate down
        if (IsFalling())
        {
            rotation -= _rotatingDownSpeed * Time.deltaTime;
            if (rotation < _downwardAngle)
            {
                rotation = _downwardAngle;
            }
        }

        if (_velocity.y > _maxVecY)
            _velocity.y = _maxVecY;

        _velocity = _velocity + _acceleration * Time.deltaTime;
        _position = _trans.position + _velocity * Time.deltaTime;

        if (_position.y > _maxHeightY)
            _position.y = _maxHeightY;

        _trans.position = _position;
        _trans.rotation = Quaternion.Euler(0f, 0f, rotation);
    }

    public void FlyUp(float buffUpwardY = 0f)
    {
        if (!_canFly)
            return;

        _velocity.x = _forwardVecX;
        _velocity.y = _upwardVecY + buffUpwardY;
    }

    public void SetFlyForwardSpeed(float value)
    {
        _forwardVecX = value;
    }

    public void StartFlying()
    {
        _canFly = true;
    }

    public void StopFlying()
    {
        _canFly = false;
    }

    /// <summary>
    /// Auto flap bird to defined destination.
    /// </summary>
    /// <param name="destination">Destination.</param>
    /// <param name="travelTime">Travel time.</param>
    /// <param name="flappyY">Flappy y.</param>
    /// <param name="flappyTime">Flappy time.</param>
    public void FlappyToPosition(Vector3 destination, float travelTime, float flappyY, float flappyTime, Callback completeCallback = null)
    {
        LeanTween.moveLocalY(_graphicGO, _graphicTrans.position.y + flappyY, flappyTime).setEase(LeanTweenType.easeOutSine).setOnComplete(() =>
        {
            LeanTween.moveLocalY(_graphicGO, _graphicTrans.position.y - flappyY, flappyTime).setEase(LeanTweenType.easeOutSine).setLoopPingPong();
        });

        RotateUpward(flappyTime).setOnComplete(() =>
        {
            RotateDownward(flappyTime).setLoopPingPong();
        });

        LeanTween.move(gameObject, destination, travelTime).setEase(LeanTweenType.easeInOutSine).setOnComplete(() =>
        {
            LeanTween.cancel(_graphicGO);
            _graphicTrans.rotation = Quaternion.identity;
            if (completeCallback != null)
                completeCallback.Invoke();
        });
    }

    private bool IsFalling()
    {
        return _velocity.y < _fallingDownVecY;
    }

    private LTDescr RotateUpward(float duration)
    {
        return LeanTween.rotateZ(_graphicGO, _upwardAngle + 40f, duration);
    }

    private LTDescr RotateDownward(float duration)
    {
        return LeanTween.rotateZ(_graphicGO, _downwardAngle + 40f, duration);
    }
}
