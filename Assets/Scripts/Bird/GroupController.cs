﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(GroupMovement))]
public class GroupController : MonoBehaviour
{
    [SerializeField]
    private GameObject _birdPrefab;

    public int MemberNumber
    {
        get { return _listMember.Count; }
    }

    private GroupMovement _movement;
    [SerializeField]
    private List<BirdController> _listMember;

    public static UnityAction OnNoMemberInGroup;

    void OnEnable()
    {
        ActorHealth.OnActorDie += OnBirdDie;
        BirdCage.OnCreateBirdFromCage += OnCreateBirdFromCage;
        GameStateManager.OnGameStateChange += OnGameStateChange;
    }

    void OnDisable()
    {
        ActorHealth.OnActorDie -= OnBirdDie;
        BirdCage.OnCreateBirdFromCage -= OnCreateBirdFromCage;
        GameStateManager.OnGameStateChange -= OnGameStateChange;
    }

    private void OnBirdDie(GameObject actor)
    {
        if (actor.tag.CompareTo(TagCollection.BIRD) != 0)
            return;

        BirdController birdController = actor.GetComponent<BirdController>();
        birdController.Die();
        StartCoroutine(LeaveGroup(birdController, 3f));
    }

    private void OnCreateBirdFromCage(BirdController birdController)
    {
        JoinGroup(birdController);
    }

    private void OnGameStateChange(GameState prev, GameState next)
    {
        if(next == GameState.BUFF_TIME)
        {
            CreateInitBirds(3, 2f, 2f);
            _movement.SetMemberList(_listMember);
            _movement.StartFlappyStandStillMovement(0.85f);
        }
        else if(next == GameState.PLAY_NORMAL)
        {
            _movement.StopStandStillMovement();
        }
    }

    // Use this for initialization
    void Awake()
    {
        _listMember = new List<BirdController>();
        _movement = GetComponent<GroupMovement>();
    }

    public void CreateInitBirds(int amount, float rangeX, float rangeY)
    {
        JoinGroup(PoolFactory.Spawn(_birdPrefab, gameObject).GetComponent<BirdController>());

        for (int i = 1; i < amount; i++)
        {
            Vector3 pos = _listMember[i - 1].MovementComponent.Position;
            pos.x -= Random.Range(0f, rangeX);
            
            if(i % 2 == 0)
            {
                pos.y -= Random.Range(0f, rangeY);
            }
            else
            {
                pos.y += Random.Range(0f, rangeY);
            }

            GameObject birdGO = PoolFactory.Spawn(_birdPrefab, pos, gameObject);
            JoinGroup(birdGO.GetComponent<BirdController>());
        }
    }

    public void JoinGroup(BirdController birdController)
    {
        _listMember.Add(birdController);
        birdController.SetId(_listMember.Count - 1);
    }

    public IEnumerator LeaveGroup(BirdController birdController, float delay = 0f)
    {
        if (delay > 0f)
        {
            yield return new WaitForSeconds(delay);
        }

        _listMember.Remove(birdController);

        if (_listMember.Count == 0)
            if (OnNoMemberInGroup != null)
                OnNoMemberInGroup();
    }

    /// <summary>
    /// Loop through member list with index
    /// </summary>
    /// <param name="callback"></param>
    public void LoopMemberList(Callback<BirdController, int> callback)
    {
        for (int i = 0; i < _listMember.Count; i++)
        {
            callback.Invoke(_listMember[i], i);
        }
    }

    public GameObject GetMemberGO(int index)
    {
        if (index < 0 || index >= _listMember.Count)
            throw new UnityException("Out of bound member list");

        return _listMember[index].gameObject;
    }
}
