﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour
{
    [SerializeField]
    private int _birdId;

    private FlappyMovement _flappyMovement;
    private GraphicController _graphicController;
    private CircleCollider2D _collider;

    public FlappyMovement MovementComponent
    {
        get { return _flappyMovement; }
    }

    public GraphicController GraphicComponent
    {
        get { return _graphicController; }
    }

    public int BirdID
    {
        get { return _birdId; }
    }

    void OnEnable()
    {
        _collider.enabled = true;
    }

    // Use this for initialization
    void Awake()
    {
        _flappyMovement = GetComponent<FlappyMovement>();
        _graphicController = GetComponentInChildren<GraphicController>();
        _collider = GetComponent<CircleCollider2D>();
    }

    public void SetId(int id)
    {
        _birdId = id;
    }

    public void Flap(float buffUpwardY = 0f)
    {
        _flappyMovement.FlyUp(buffUpwardY);
        _graphicController.FlyUp();
    }

    public void Falling()
    {
        _flappyMovement.Falling();
    }

    public void Die()
    {
        _graphicController.Die();
        _flappyMovement.StopFlying();
        _collider.enabled = false;
        PoolFactory.Despawn(gameObject, 3f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (TagCollection.Compare(other.tag, TagCollection.BOTTOM_GROUND))
        {
            Flap();
        }
        else if (TagCollection.Compare(other.tag, TagCollection.TOP_GROUND))
        {
            _flappyMovement.StopFlying();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (TagCollection.Compare(other.tag, TagCollection.TOP_GROUND))
        {
            _flappyMovement.StartFlying();
        }
    }
}
