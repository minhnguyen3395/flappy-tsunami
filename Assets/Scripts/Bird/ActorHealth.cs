﻿using UnityEngine;
using UnityEngine.Events;

public class ActorHealth : MonoBehaviour
{
    [SerializeField]
    private int _healthCount = 1;

    [SerializeField]
    private LayerMask _enemyLayerMask;

    public static UnityAction<GameObject> OnLoseHealth;
    public static UnityAction<GameObject> OnActorDie;

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((_enemyLayerMask.value & 1 << other.gameObject.layer) != 0)
        {
            _healthCount--;

            // Do something
            if (_healthCount == 0)
            {
                if (OnActorDie != null)
                    OnActorDie(gameObject);
            }
            else if (_healthCount > 0)
            {
                if (OnLoseHealth != null)
                    OnLoseHealth(gameObject);
            }
        }
    }
}
