﻿using UnityEngine;
using Lean;
using System.Collections.Generic;
using System.Collections;

public class GroupMovement : MonoBehaviour
{
    [SerializeField]
    private float _delaySecsBetweenMember = 0.02f;

    [SerializeField]
    private float _buffFactorForLowerHeightMember = 0.5f;

    private List<BirdController> _memberList;
    private WaitForSeconds _waitForSecs;
    private Coroutine _updateMovementCor;
    private Coroutine _updateStandStillMovementCor;

    void OnEnable()
    {
        InputManager.OnFlyUp += OnFlyUp;
    }

    void OnDisable()
    {
        InputManager.OnFlyUp -= OnFlyUp;
    }

    void Awake()
    {
        _waitForSecs = new WaitForSeconds(_delaySecsBetweenMember);
    }

    public void SetMemberList(List<BirdController> members)
    {
        _memberList = members;
    }

    public void StartUpdateMemberMovement()
    {
        _updateMovementCor = StartCoroutine(UpdateGroupMovement((BirdController controller) =>
        {
            controller.Falling();
        }, null));
    }

    public void StartFlappyStandStillMovement(float updatePeriod)
    {
        StartUpdateMemberMovement();
        WaitForSeconds waitSecs = new WaitForSeconds(updatePeriod);

        _updateStandStillMovementCor = StartCoroutine(UpdateGroupMovement((BirdController controller) =>
        {
            controller.Flap();
        }, waitSecs));
    }

    public void StopUpdateFallingMovement()
    {
        StopCoroutine(_updateMovementCor);
    }

    public void StopStandStillMovement()
    {
        StopCoroutine(_updateStandStillMovementCor);
    }

    private IEnumerator UpdateGroupMovement(Callback<BirdController> controller, WaitForSeconds waitSecs)
    {
        while (true)
        {
            for (int i = 0; i < _memberList.Count; i++)
            {
                controller.Invoke(_memberList[i]);
            }

            yield return waitSecs;
        }
    }

    private void OnFlyUp()
    {
        StartCoroutine(BroadcastToMemberCor());
    }

    private IEnumerator BroadcastToMemberCor()
    {
        MinPositionYFirstList();
        for (int i = 0; i < _memberList.Count; i++)
        {
            _memberList[i].Flap(_buffFactorForLowerHeightMember * i);
            yield return _waitForSecs;
        }
    }

    /// <summary>
    /// Sort member list by descending position y order.
    /// </summary>
    private void MinPositionYFirstList()
    {
        int size = _memberList.Count;
        for (int i = 0; i < size; i++)
        {
            for (int k = 0; k < size - 1; k++)
            {
                if (_memberList[k].MovementComponent.Position.y < _memberList[k + 1].MovementComponent.Position.y)
                {
                    BirdController tmp = _memberList[k];
                    _memberList[k] = _memberList[k + 1];
                    _memberList[k + 1] = tmp;
                }
            }
        }
    }
}
