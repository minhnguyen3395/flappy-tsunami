﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class DifficultyManager : MonoBehaviour
{
	[SerializeField]
	private Difficulty[] _difficulties;

	private Difficulty _currentDif;

	public Difficulty CurrentDifficulty { get { return _currentDif; } }

	private int _currentIndex = -1;

	public static UnityAction<Difficulty> OnDifficultyChange;

	void Start ()
	{
		ChangeToNextDifficulty ();
	}

	private void ChangeToNextDifficulty ()
	{
		_currentIndex++;
		_currentDif = _difficulties [_currentIndex];

		if (OnDifficultyChange != null)
			OnDifficultyChange (_currentDif);
	}

	[System.Serializable]
	public class Difficulty : System.Object
	{
		[SerializeField]
		private int _requiredScore;

		public int RequiredScore {
			get { return _requiredScore; }
		}

		[SerializeField]
		private float _spawnObstaclePeriod;

		public float SpawnObstaclePeriod {
			get { return _spawnObstaclePeriod; }
		}

		[SerializeField]
		private float _obstacleMovingDuration;

		public float ObstacleMovingDuration {
			get{ return _obstacleMovingDuration; }
		}
	}
}
