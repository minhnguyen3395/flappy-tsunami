﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public enum GameState
{
    BUFF_TIME, PLAY_NORMAL, PLAY_BOSS_FIGHT, END_GAME
}

public class GameStateManager : UnitySingleton<GameStateManager>
{
	[SerializeField]
	private GameState _currentState;

	/// <summary>
	/// Game state change event. Send to listener prev state and next state
	/// </summary>
	public static UnityAction<GameState, GameState> OnGameStateChange;

    void OnEnable()
    {
        GroupController.OnNoMemberInGroup += OnGameOver;
    }

    void OnDisable()
    {
        GroupController.OnNoMemberInGroup -= OnGameOver;
    }

    protected override void Awake()
    {
        base.Awake();
        ChangeGameState(GameState.BUFF_TIME);
    }

    private void OnGameOver()
    {
        ChangeGameState(GameState.END_GAME);
    }

    public void ChangeGameState(GameState newState)
	{
		if (OnGameStateChange != null)
			OnGameStateChange (_currentState, newState);
		
		_currentState = newState;
	}
}
