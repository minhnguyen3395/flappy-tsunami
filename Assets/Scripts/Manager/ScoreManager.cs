﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ScoreManager : MonoBehaviour {
    private float _scorePerSec;
    private float _currentScore;

    private Coroutine _scoreCor;
    private WaitForSeconds _waitSec;

    public static UnityAction<float> OnScoreUpdate;

    void OnEnable()
    {
        GameStateManager.OnGameStateChange += OnGameStateChange;
        DifficultyManager.OnDifficultyChange += OnDifficultyChange;
    }

    void OnDisable()
    {
        GameStateManager.OnGameStateChange -= OnGameStateChange;
        DifficultyManager.OnDifficultyChange -= OnDifficultyChange;
    }

    void Awake()
    {
        _waitSec = new WaitForSeconds(1f);
    }

    private void OnGameStateChange(GameState prev, GameState next)
    {
        if(next == GameState.PLAY_NORMAL)
        {
            _scoreCor = StartCoroutine(CalculateScoreCoroutine());
        }
        else 
        {
            
        }
    }

    private void OnDifficultyChange(DifficultyManager.Difficulty diff)
    {
        _scorePerSec = diff.ObstacleMovingDuration / ViewportHandler.Instance.WidthInMeter;
    }

    private IEnumerator CalculateScoreCoroutine()
    {
        while(true)
        {
            _currentScore += _scorePerSec;

            if (OnScoreUpdate != null)
                OnScoreUpdate(_currentScore);
            yield return _waitSec;
        }
    }
}
