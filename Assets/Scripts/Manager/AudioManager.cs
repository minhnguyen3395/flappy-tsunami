﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : UnitySingletonPersistent<AudioManager>
{
    [System.Serializable]
    public class AudioClipInfo : System.Object
    {
        public AudioClip audioClip;
        public ClipInfoHelper.UNIQUE_KEY uniqueKey;
        public AudioSource audioSource;
    }

    public enum CLIPTYPE
    {
        BACKGROUND, TEMPORARY
    };

    public AudioSource _uiAudioSource;
    public AudioClipInfo[] _backgroundAudios;
    public AudioClipInfo[] _tmpAudios;

    private Dictionary<ClipInfoHelper.UNIQUE_KEY, int> _clipInfoDict;

	protected override void Awake()
    {
		base.Awake ();
        PreProcessingAudioArray();
    }

    private void PreProcessingAudioArray()
    {
        _clipInfoDict = new Dictionary<ClipInfoHelper.UNIQUE_KEY, int>();

        for (int i = 0; i < _backgroundAudios.Length; i++)
        {
            _clipInfoDict.Add(_backgroundAudios[i].uniqueKey, i);
        }

        for (int i = 0; i < _tmpAudios.Length; i++)
        {
            _clipInfoDict.Add(_tmpAudios[i].uniqueKey, i);
        }
    }

    #region PLAY AUDIO
    public AudioSource PlayLoop(CLIPTYPE type, ClipInfoHelper.UNIQUE_KEY key)
    {

        AudioClipInfo clipInfo = GetAudioClipInfo(type, key);

        if (clipInfo == null)
            return null;

        clipInfo.audioSource.Stop();
        clipInfo.audioSource.clip = clipInfo.audioClip;
        clipInfo.audioSource.Play();

        return clipInfo.audioSource;
    }

    public AudioSource PlayOne(CLIPTYPE type, ClipInfoHelper.UNIQUE_KEY key)
    {
        AudioClipInfo clipInfo = GetAudioClipInfo(type, key);

        if (clipInfo == null)
            return null;

        clipInfo.audioSource.Stop();
        clipInfo.audioSource.PlayOneShot(clipInfo.audioClip);
        return clipInfo.audioSource;
    }

    public void PlayOne(AudioClip clip)
    {
        _uiAudioSource.PlayOneShot(clip, 1f);
    }

    public AudioSource PlayOneCustomSource(CLIPTYPE type, ClipInfoHelper.UNIQUE_KEY key, AudioSource customSource)
    {

        AudioClipInfo clipInfo = GetAudioClipInfo(type, key);

        if (clipInfo == null)
            return null;

        clipInfo.audioSource = customSource;
        clipInfo.audioSource.Stop();
        clipInfo.audioSource.PlayOneShot(clipInfo.audioClip);
        return clipInfo.audioSource;
    }
#endregion

    #region STOP AUDIO
    public void StopClip(CLIPTYPE type, ClipInfoHelper.UNIQUE_KEY key)
    {

        AudioClipInfo clipInfo = GetAudioClipInfo(type, key);

        if (clipInfo == null)
            return;

        clipInfo.audioSource.Stop();
        clipInfo.audioSource.clip = null;
    }

    public void StopAllSound()
    {
        for (int i = 0; i < _backgroundAudios.Length; i++)
        {
            _backgroundAudios[i].audioSource.Stop();
        }

        for (int i = 0; i < _tmpAudios.Length; i++)
        {
            _tmpAudios[i].audioSource.Stop();
        }
    }

    public void PauseAllSound()
    {
        for (int i = 0; i < _backgroundAudios.Length; i++)
        {
            _backgroundAudios[i].audioSource.Pause();
        }

        for (int i = 0; i < _tmpAudios.Length; i++)
        {
            _tmpAudios[i].audioSource.Pause();
        }
    }

    public void UnpauseAllSound()
    {
        for (int i = 0; i < _backgroundAudios.Length; i++)
        {
            _backgroundAudios[i].audioSource.UnPause();
        }

        for (int i = 0; i < _tmpAudios.Length; i++)
        {
            _tmpAudios[i].audioSource.UnPause();
        }
    }
    # endregion

    # region GET METHODE
    private AudioClipInfo GetAudioClipInfo(CLIPTYPE type, ClipInfoHelper.UNIQUE_KEY key)
    {
        AudioClipInfo[] clipArray = type == CLIPTYPE.BACKGROUND ? _backgroundAudios : _tmpAudios;

        if (!_clipInfoDict.ContainsKey(key))
            return null;

        int index = _clipInfoDict[key];

        return clipArray[index];
    }

    public AudioClipInfo GetRandomBackgroundClipInfo()
    {
        return _backgroundAudios[Random.Range(0, _backgroundAudios.Length)];
    }

    public AudioClipInfo GetRandomTemporaryClipInfo()
    {
        return _tmpAudios[Random.Range(0, _tmpAudios.Length)];
    }
    # endregion
}
