﻿using UnityEngine;

public class PoolFactory
{
    public static GameObject Spawn(GameObject prefab)
    {
        return TrashMan.spawn(prefab);
    }

	public static GameObject Spawn (GameObject prefab, Vector3 position, Quaternion rotation)
	{
		return  TrashMan.spawn (prefab, position, rotation);
	}

	public static GameObject Spawn (GameObject prefab, Vector3 position, GameObject parent)
	{
		GameObject go = Spawn (prefab, position, Quaternion.identity);
		go.transform.parent = parent.transform;
		return go;
	}

    /// <summary>
    /// Spawn game object at local position of parent
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="parent"></param>
    /// <param name="localPosition"></param>
    /// <returns></returns>
    public static GameObject Spawn(GameObject prefab, GameObject parent, Vector3 localPosition)
    {
        GameObject go = Spawn(prefab, Vector3.zero, Quaternion.identity);
        go.transform.parent = parent.transform;
        go.transform.localPosition = localPosition;
        return go;
    }

    /// <summary>
    /// Spawn game object at (0, 0, 0) of parent.
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public static GameObject Spawn(GameObject prefab, GameObject parent)
    {
        GameObject go = Spawn(prefab, Vector3.zero, Quaternion.identity);
        go.transform.parent = parent.transform;
        go.transform.localPosition = Vector3.zero;
        return go;
    }

    public static void Despawn (GameObject go)
	{
		TrashMan.despawn (go);
	}

	public static void Despawn (GameObject go, float delay)
	{
		TrashMan.despawnAfterDelay (go, delay);
	}
}
