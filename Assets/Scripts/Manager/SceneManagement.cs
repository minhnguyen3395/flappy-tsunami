﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Collections.Generic;

public class SceneManagement : UnitySingletonPersistent<SceneManagement>
{
	#region VAR
	public enum SceneID
	{
		INTRO,
		MENU,
		WORDGAME,
		CARGAME,
		WORDDRAWGAME,
		CARGAME2,
		STARTUP
	}

	private Dictionary<SceneID, string> _sceneDict;

	#endregion

	public static UnityAction OnStartLoading;
	public static UnityAction OnEndLoading;
	public static UnityAction<float> OnLoadingScene;

	protected override void Awake ()
	{
		base.Awake ();
	}

	public void ReloadCurrentScene ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void LoadingSceneAsync (SceneID id, float delay = 0f)
	{
		StartCoroutine (LoadingOperation (id, delay));
	}

	private IEnumerator LoadingOperation (SceneID id, float delay)
	{
		if (delay > 0f)
			yield return new WaitForSeconds (delay);

		if (OnStartLoading != null)
			OnStartLoading ();
		
		AsyncOperation async = SceneManager.LoadSceneAsync ((int)id);

		async.allowSceneActivation = false;	

		float averagePercent = 0f;
		while (true) {
			if (OnLoadingScene != null)
				OnLoadingScene (averagePercent);	
			

			if (averagePercent == 0.99f) {
				yield return new WaitForSeconds (1f);

				if (OnEndLoading != null)
					OnEndLoading ();
				
				async.allowSceneActivation = true;
				yield break;
			}
			
			yield return null;
		}
	}
}
