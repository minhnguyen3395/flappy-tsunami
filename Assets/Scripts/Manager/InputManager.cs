﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Lean;

public class InputManager : UnitySingleton<InputManager>
{
	public static UnityAction OnFlyUp;

    private Callback<LeanFinger> OnFingerDownCallback;

    protected override void Awake()
    {
        base.Awake();
    }

	void OnEnable()
	{
		LeanTouch.OnFingerDown += OnFingerDown;
        GameStateManager.OnGameStateChange += OnGameStateChange;
	}

	void OnDisable()
	{
		LeanTouch.OnFingerDown -= OnFingerDown;
        GameStateManager.OnGameStateChange -= OnGameStateChange;
    }

    private void OnGameStateChange(GameState prev, GameState next)
    {
        if(next == GameState.BUFF_TIME)
        {
            OnFingerDownCallback = OnFingerDown_StartGame;

        }
        else if(next == GameState.PLAY_NORMAL || next == GameState.PLAY_BOSS_FIGHT)
        {
            OnFingerDownCallback = OnFingerDown_FlyUp;

        }
    }

    #region LeanTouch Callback
    private void OnFingerDown(LeanFinger finger)
	{
		if (LeanTouch.GuiInUse)
			return;

        OnFingerDownCallback.Invoke(finger);
	}
    #endregion

    #region Game Input Callback
    private void OnFingerDown_FlyUp(LeanFinger finger)
    {
        if (OnFlyUp != null)
            OnFlyUp();
    }

    private void OnFingerDown_StartGame(LeanFinger finger)
    {
        GameStateManager.Instance.ChangeGameState(GameState.PLAY_NORMAL);
    }
    #endregion
}
