﻿using UnityEngine;
using System.Collections;

public class ObstacleManager : MonoBehaviour
{
	[SerializeField]
	private Transform _leftTop;

	[SerializeField]
	private Transform _leftMid;

	[SerializeField]
	private Transform _leftBot;

	[SerializeField]
	private Transform _rightTop;

	[SerializeField]
	private Transform _rightMid;

	[SerializeField]
	private Transform _rightBot;

	[SerializeField]
	private GameObject[] _staticPatterns;

	[SerializeField]
	private GameObject[] _dynamicObstacles;

	private DifficultyManager.Difficulty _curDiff;
	private WaitForSeconds _waitForSecs;

	void OnEnable ()
	{
        GameStateManager.OnGameStateChange += OnGameStateChange;
		DifficultyManager.OnDifficultyChange += OnDifficultyChange;
	}

	void OnDisable ()
	{
		DifficultyManager.OnDifficultyChange -= OnDifficultyChange;
        GameStateManager.OnGameStateChange -= OnGameStateChange;
    }

    private void OnGameStateChange(GameState prev, GameState next)
    {
        if(next == GameState.PLAY_NORMAL)
        {
            StartCoroutine(SpawnObstacleCoroutine());
        }
    }

	public void SpawnMiddle ()
	{
		Vector3 pos = _rightMid.position;
		pos.x = pos.x + ViewportHandler.Instance.Width / 2;
		GameObject staticObstacle = PoolFactory.Spawn (GetRandomStaticPattern (), pos, Quaternion.identity);
		ObstacleController controller = staticObstacle.GetComponent<ObstacleController> ();
		controller.StartMoving (_leftMid, _curDiff.ObstacleMovingDuration);
	}

	private void OnDifficultyChange (DifficultyManager.Difficulty currentDiff)
	{
		_curDiff = currentDiff;
		_waitForSecs = new WaitForSeconds (_curDiff.SpawnObstaclePeriod);
	}

	private IEnumerator SpawnObstacleCoroutine ()
	{
		while (true) {
			if (_curDiff != null)
				SpawnMiddle ();
			yield return _waitForSecs;
		}
	}

	private GameObject GetRandomStaticPattern ()
	{
		return _staticPatterns [Random.Range (0, _staticPatterns.Length)];
	}
}
