﻿using UnityEngine;
using UnityEngine.UI;
public class GUIManager : UnitySingletonPersistent<GUIManager>
{
	[SerializeField]
	private UIDialog _dialog;	

	public UIDialog OpenDialog (string message, params Sprite[] uiSprites)
	{
		_dialog.gameObject.SetActive (true);
		_dialog.RefreshDialog ();
		_dialog.SetMessageText (message, uiSprites);
		return _dialog;
	}

	protected override void Awake ()
	{
		base.Awake ();
	}

    public void FadeIn(Image image, float duration)
    {
        LeanTween.alpha(image.rectTransform, 1f, duration);
    }

    public void FadeOut(Image image, float duration)
    {
        LeanTween.alpha(image.rectTransform, 0f, duration);
    }
}
