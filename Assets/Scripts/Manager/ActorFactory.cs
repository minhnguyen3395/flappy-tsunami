﻿using UnityEngine;
using System.Collections;

public class ActorFactory : UnitySingleton<ActorFactory> {
    [SerializeField]
    private GameObject _groupBird;

	[SerializeField]
	private GameObject _birdPrefab;

	public GameObject InstantiateBird(Vector3 position)
	{
		GameObject birdGO = PoolFactory.Spawn (_birdPrefab, position, _groupBird);

		return birdGO;
	}
}
