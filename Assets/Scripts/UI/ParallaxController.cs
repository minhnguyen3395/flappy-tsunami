﻿using UnityEngine;
using System.Collections;

public class ParallaxController : MonoBehaviour
{
	public FreeParallax _parallax;
	public float _parallaxRootSpeed = -15f;
	
	// Update is called once per frame
	void Update ()
	{
		_parallax.Speed = _parallaxRootSpeed;
	}
}
