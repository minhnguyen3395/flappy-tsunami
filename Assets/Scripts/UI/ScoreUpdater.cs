﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdater : MonoBehaviour {
    private Text _scoreText;

    void OnEnable()
    {
        ScoreManager.OnScoreUpdate += OnScoreUpdate;
    }

    void OnDisable()
    {
        ScoreManager.OnScoreUpdate -= OnScoreUpdate;
    }

    void Awake()
    {
        _scoreText = GetComponent<Text>();
    }

    private void OnScoreUpdate(float score)
    {
        _scoreText.text = score.ToString();
    }
}
