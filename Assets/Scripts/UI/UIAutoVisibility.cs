﻿using UnityEngine;
using UnityEngine.UI;

public class UIAutoVisibility : MonoBehaviour {

    public GameState[] _hideState;

    public GameState[] _showState;

    private Image _image;

    void OnEnable()
    {
        GameStateManager.OnGameStateChange += OnGameStateChange;
    }

    void OnDisable()
    {
        GameStateManager.OnGameStateChange -= OnGameStateChange;
    }

    void Awake()
    {
        _image = GetComponent<Image>();
    }

    private void OnGameStateChange(GameState prev, GameState next)
    {
        for(int i = 0; i < _hideState.Length; i++)
        {
            if(next == _hideState[i])
            {
                GUIManager.Instance.FadeOut(_image, 0.5f);
                return;
            }
        }

        for (int i = 0; i < _showState.Length; i++)
        {
            if (next == _showState[i])
            {
                GUIManager.Instance.FadeIn(_image, 0.5f);
                return;
            }
        }
    }
}
